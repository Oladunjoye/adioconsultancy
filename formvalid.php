<?php
class ValidateForm {

  public static function valid ($param){
$name =""; // Sender Name

$email =""; // Sender's email ID

$message =""; // Sender's Message

$nameError ="";

$emailError ="";

$coverletterError ="";

$fileError ="";

$successMessage =""; // On submitting form below function will execute.

if ( isset ( $_POST['submit'] ) )  { // Checking null values in message.

if ( empty ( $_POST["name1"] ) ) {

$nameError = "First name is required";

}
if ( empty ( $_POST["name2"] ) ) {

$nameError = "Surname is required";

}

if ( empty ( $_POST["email"] ) ) {

$emailError = "Email is required";

}
if ( empty ( $_POST["coverletter"] ) ) {

$coverletterError = "coverletter is required";

}
if (! in_array($fileExtension,$fileExtensions)) {
    $fileError = "This file extension is not allowed. Please upload a JPEG or PNG file";
}

if ($fileSize > 2000000) {
    $fileError = "This file is more than 2MB. Sorry, it has to be less than or equal to 2MB";
}

if ( !$_FILES['passport']['name'] ) {

 $fileError = "File is required";
 }
if ( !$_FILES['resume']['name'] ) {

 $fileError = "File is required";

 }

}

// DATABSE ENTRY
