<?php

session_start();
include"db.php";

if (isset($_POST['email']) && isset($_POST['password'])  ) {
	echo "yoooo";
 	try{

 		// QUERY COMMANDS: SELECT ALL DATA
 		$query = "SELECT name FROM users WHERE email=:email AND password=:password";
 		$stmt = $connection->prepare($query);
 		// $result = mysqli_query($connection,$query);

 		// bind the parameters
 		$email = $_POST['email'];
 		$password = hash('sha256',$_POST['password']);

 		$stmt->bindParam(':email', $email);
 		$stmt->bindParam(':password', $password);
 		$stmt->execute();

 		$num = $stmt->rowCount();

 		if($num > 0){
 			$row = $stmt->fetch();
 			$_SESSION['name'] = $row['name'];
 			header("Location: ../explore.php");
 			exit(); 
 		}
 	}

 	catch(PDOException $exception){
 		die('ERROR:' . $exception->getMessage());

 	}
 }

 else {
 	session_destroy();
 } 
 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title></title>
 	<link rel="stylesheet" href="../css/bootstrap.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
   <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400" rel="stylesheet">

   <style type="text/css">
   	.login-form{
   		margin-top: 30px;
   		padding-bottom: 30px;
   		border-bottom:2px solid black;

   	}

   	.more-info {
   		
   		padding-top: 15px;
   		background: #eee;
   	}
   </style>
 </head>
 <body>
  <!-- NAVBAR SECTION -->
        <div class=" row navbars " style="max-width: 100%">
          <nav class="collapse.navbar-collapse">
            <i class="fab fa-500px logo"></i>
            <ul class="main-nav">
              <li><a href="../home.php">Home</a></li>
              <li><a href="../signup.php">Sign Up</a></li>
              <li><a href="login.php">Login</a></li>


            </ul>

          </nav>
        </div>


    <!-- NAVBAR ENDS -->


 	<!-- LOGIN FORM -->
 	<div class="container-fluid login-form">
 	<form method="POST" action="login.php">
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password" required>
  </div>

  <p><h8>Dont have an account? <a href="../signup.php"> Sign Up </a></h5> </p>
  <!-- <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> -->
  <input type="submit" class="btn btn-primary" name="submit">
  
</form>
 
 </div>

 <!-- BOTTOM SECTION -->
 <section class="more-info">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4">

            <ul>
              <li><h5>Quick links</h5></li>
              <li><a href="index.html">Home</a></li>
              <li><a href="signup.html">Sign Up</a></li>
              <li><a href="explore.html">Gallery</a></li>
            </ul>
          </div>
          <div class="col-md-4">

                  <ul>
                    <li><h5>Social</h5></li>
                    <li><a href=""><i class="fab fa-facebook-square"></i></a></li>
                    <li><a href=""><i class="fab fa-twitter-square"></i></a></li>
                    <li><a href=""><i class="fab fa-instagram"></i></a></li>
                  </ul>
            </div>
          <div class="col-md-4">

            <ul>
              <li><h5>Company</h5></li>
              <li><a href="">FAQ</a></li>
              <li><a href="">Get Help</a></li>
              <li><a href="">Status</a></li>
            </ul>
          </div>


        </div>
      </div>
    </section>

    <footer class="footer" style="margin-bottom: 0;">
      <div class=" row navbars" style="max-width: 100%">
        <nav>

          <i class="fab fa-500px logo"></i>
          <ul class="main-nav">

            <li><a href="index.html">Copyright &copy; 2018 </a></li>
            <li><a href="signup.html">Terms</a></li>
            <li><a href="explore.html">Privacy</a></li>

          </ul>
        </nav>
      </div>
    </footer>



<!-- <script src="js/jquery-3.3.1.js" charset="utf-8"></script>
<script src="js/formValidate.js" charset="utf-8"></script>
<script src="js/bootstrap.js" charset="utf-8"></script> -->


<script src="https://unpkg.com/ionicons@4.2.4/dist/ionicons.js"></script>

 </body>
 </html>